const { I } = inject();

Given(/I open page tiket.com/, () => {
    I.amOnPage('https://www.tiket.com/');
    I.wait(10);
});

Given(/I choose sewa mobil/, () => {
    I.click('.product-list .product-link:nth-child(4) a');
    I.wait(10);
});

Given(/I choose kota/, () => {
    I.executeScript(function () {
        var name = "DKI Jakarta";
        var ids = document.querySelectorAll('.jstarget-all-dropdownlocation .listing a');
        var names = document.querySelectorAll('.jstarget-all-dropdownlocation .listing .regional-name');

        for (var i = 0; i < ids.length; i += 1) {
            var region = {
                id: ids[i].getAttribute('data-value'),
                name: names[i].innerHTML
            }

            if (region.name.toLowerCase().includes(name.toLowerCase()) > 0) {
                document.querySelector('#search-regionalarea').value = region.name;
                document.querySelector('#regionalarea').value = region.id;
                break;
            }
        }
        // now we are inside browser context
    });
    I.wait(10);
});

When(/I click Submit "pencarian sewa"/, () => {
    I.click('.search-car-container a');
    I.wait(10);
});

Then(/success view list price sewa mobil/, () => {
    I.see('Filter');
});



