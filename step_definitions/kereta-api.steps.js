const { I } = inject();

Given(/I open page tiket.com/, () => {
    I.amOnPage('https://www.tiket.com/');
    I.wait(10);
});

Given(/I choose kereta api/, () => {
    I.click('.product-list-container .product-list-content .product-box:nth-child(3)');
    I.wait(10);
});

Given(/I choose berangkat dari/, () => {
    I.executeScript(function () {
        var station = "bandung";
        var ids = document.querySelectorAll('.jstarget-all-station .listing a');
        var stations = document.querySelectorAll('.jstarget-all-station .listing .station-name');
        var labels = document.querySelectorAll('.jstarget-all-station .listing .hidden-label');
        //var names = document.querySelectorAll('.downleft.jstarget-all-station .listing .station-name');

        for (var i = 0; i < ids.length; i += 1) {
            var data = {
                id: ids[i].getAttribute('data-value'),
                type: ids[i].getAttribute('data-type'),
                station: stations[i].innerHTML,
                label: labels[i].innerHTML
            }

            if (data.station.toLowerCase().includes(station.toLowerCase()) > 0) {
                console.log(data);
                document.querySelector('#train-search-d').value = data.label;
                document.querySelector('#train-d').value = data.id;
                document.querySelector('#train-dt').value = data.type;
                break;
            }
        }
        // now we are inside browser context
    });
    I.wait(10);
});

Given(/I choose tujuan ke/, () => {
    I.executeScript(function () {
        var station = "jakarta";
        var ids = document.querySelectorAll('.jstarget-all-station .listing a');
        var stations = document.querySelectorAll('.jstarget-all-station .listing .station-name');
        var labels = document.querySelectorAll('.jstarget-all-station .listing .hidden-label');
        //var names = document.querySelectorAll('.downleft.jstarget-all-station .listing .station-name');

        for (var i = 0; i < ids.length; i += 1) {
            var data = {
                id: ids[i].getAttribute('data-value'),
                type: ids[i].getAttribute('data-type'),
                station: stations[i].innerHTML,
                label: labels[i].innerHTML
            }

            if (data.station.toLowerCase().includes(station.toLowerCase()) > 0) {
                console.log(data);
                document.querySelector('#train-search-a').value = data.label;
                document.querySelector('#train-a').value = data.id;
                document.querySelector('#train-at').value = data.type;
                break;
            }
        }
        // now we are inside browser context
    });
});

When(/I klik search/, () => {
    I.click('.js-check-date .button-primary');
    I.wait(15);
});

Then(/success view list kereta api available/, () => {
    I.see('Kami telah menampilkan semua jadwal kereta untuk rute ini');
});