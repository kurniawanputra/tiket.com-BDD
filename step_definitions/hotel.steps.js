const { I } = inject();

Given(/I open page tiket.com/, () => {
    I.amOnPage('https://www.tiket.com/');
    I.wait(10);
});

Given(/I choose hotel/, () => {
    I.click('.product-list-content .product-box:nth-child(2)');
    I.wait(5);
});

Given(/I choose tujuan hotel jakarta/, () => {
    I.fillField('#productSearchDestination', 'jakarta');
    I.wait(5);
    I.click('#destinationDropDownList .ReactVirtualized__Grid__innerScrollContainer #destinationDropDownList-place1');
    I.wait(5);
});

Given(/I choose tanggal check in/, () => {
    I.click('#startDate');
    I.wait(5);
    I.executeScript(function () {
        // now we are inside browser context
        document.querySelector('#productSearchCheckIn').value = "Rab, 25 Sep 2019";
    });
    I.wait(5);
});

Given(/I choose tanggal check out/, () => {
    I.executeScript(function () {
        // now we are inside browser context
        document.querySelector('#productSearchCheckOut').value = "Sab, 28 Sep 2019";
    });
    I.wait(5);
});

Given(/I klik close date/, () => {
    I.click('.widget-drop-down-close-btn');
    I.wait(5);
});

Given(/I close kamar& tamu/, () => {
    I.click('.product-form-container.product-form-search-group .widget-hotel-guest-room .widget-drop-down .widget-drop-down-content .widget-drop-down-close-btn');
    I.wait(3);
});

When(/I klik search/, () => {
    I.click('.product-form-container.product-form-button-group button.product-form-search-btn');
    I.wait(10);
});

Then(/success view list hotel/, () => {
    I.see('Cari');
});