Behavior Driven Development for example TIket.com

Behavior Driven Development (BDD) is a popular software development methodology. 

What is Behavior Driven Development ?

outside-in, pull-based, multiple-stakeholder, multiple-scale, high-automation, agile methodology. It describes a cycle of interactions with well-defined outputs, resulting in the delivery of working, tested software that matters.

The idea of story BDD can be narrowed to:

describe features in a scenario with a formal text
use examples to make abstract things concrete
implement each step of a scenario for testing
write actual code implementing the feature
By writing every feature in User Story format that is automatically executable as a test we ensure that: business, developers, QAs and managers are in the same boat.

Each feature of a product should be born from a talk between : 

business (analysts, product owner)
developers
QA Engineer
which are known in BDD as "three amigos".

We can try to write such simple story:

"Feature: Search Tiket Pesawat
  In order to achieve my goals
  As a customer
  I want to buy ticket pesawat from City A to City B
  so expected result is view list price plane"

so for example tiket.com.
i create 5 scenario :
- i want buy ticket flight
- i want need holiday and i am booking hotel
- i want use transport via train
- i want entertainment Attractions in city a
- and I want to rent a car for a walk in the city of A

for more detail please see every feature in the project.

Thank you