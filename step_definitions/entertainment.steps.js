const { I } = inject();

Given(/I open page tiket.com/, () => {
    I.amOnPage('https://www.tiket.com/');
    I.wait(10);
});

Given(/I choose entertainment/, () => {
    //I.click('.product-list-container .product-list-content a.product-box:nth-child(5)');
    I.click('.product-list .product-link:nth-child(5) a');
    I.wait(10);
});

Given(/I click dropdown and showing view list dropdown/, () => {
    I.click('.search__category .search__category--selected .tixicon-down');
    I.wait(10);
});

Given(/I choose menu "kategori" in dropdown/, () => {
    I.click('.search__category__list .search__category__item:nth-child(3)');
    I.wait(10);
});

// Given('/I input in "" in field/', () => {
//     I.fillField('#submit_search span input[type=text]','');
//     I.wait(15);
// });

When(/I click Submit "pencarian entertainment/, () => {
    I.click('.btn.btn-primary');
    I.wait(10);
});

Then(/success view list price entertainment/, () => {
    I.see('Rentang Harga');
});

