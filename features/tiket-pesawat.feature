Feature: Search Tiket Pesawat
  In order to achieve my goals
  As a customer
  I want to buy ticket pesawat from A ke buy
  so expected result is view list price plane

  Scenario: Search tiket pesawat
    Given I open page tiket.com
    And I choose flight
    And I input "dari" flight from
    And I input "ke" flight to
    And I choose "tanggal berangkat"
    And I Unchecklist "pulang"
    And I close "penumpang & kabin"
    When I click Submit "Cari penerbangan"
    Then success view list price plane
    