const { I } = inject();

// you can provide RegEx to match corresponding steps
Given(/I open page tiket.com/, () => {
  I.amOnPage('https://www.tiket.com/');
  I.wait(10);
});

Given(/I choose flight/, () => {
    I.click('.product-list-content .product-box:nth-child(1)');
    I.wait(10);
});

Given(/I input "dari" flight from/, () => {
    I.fillField('.product-form-container:nth-child(2) .form-to-input-group:nth-child(1) .widget-input-container input[class=product-search-input]', 'DJB');
    I.click('.product-form-container:nth-child(2) .form-to-input-group:nth-child(1) .widget-drop-down .widget-drop-down-content .flight-search-airport-city #fromDropDownList .ReactVirtualized__Grid__innerScrollContainer #fromDropDownList-airport1');
    I.wait(5);
});

Given(/I input "ke" flight to/, () => {
    I.fillField('.product-form-container:nth-child(2) .form-to-input-group:nth-child(2) .widget-input-container .product-search-input', 'CGK');
    I.click('.product-form-container:nth-child(2) .form-to-input-group:nth-child(1) .widget-drop-down .widget-drop-down-content .flight-search-airport-city #fromDropDownList .ReactVirtualized__Grid__innerScrollContainer #fromDropDownList-airport1');
    I.wait(5);
});

Given(/I choose "tanggal berangkat"/, () => {
    I.click('.product-form-container:nth-child(2) .widget-date-input-group-container .widget-drop-down .widget-drop-down-content .widget-date-picker .widget-date-picker-content .DayPicker .DayPicker_focusRegion .DayPicker_transitionContainer .CalendarMonthGrid .CalendarMonthGrid_month__horizontal .CalendarMonth .CalendarMonth_table tbody tr:nth-child(4) .CalendarDay:nth-child(2) .widget-date-picker-day');
    I.wait(5);
});

Given(/I Unchecklist "pulang"/, () => {
    I.click('.product-form .product-form-container:nth-child(2) .widget-date-input-group-container .input-date-type:nth-child(2) .widget-input-container .product-search-input-label .check.v3 #productSearchReturnCheckbox');
    I.wait(5);
});

Given(/I close "penumpang & kabin"/, () => {
    I.click('#passengerCabin .widget-drop-down .widget-drop-down-content .passenger-cabin-drop-down-container .passenger-cabin-drop-down-footer .passenger-cabin-drop-down-text span');
    I.wait(10);
});

When(/I click Submit "Cari penerbangan"/, () => {
    I.click('#productWidget .product-form .product-form-container:nth-child(3) button.product-form-search-btn');
    I.wait(20);
});

Then(/success view list price plane/, () => {
  I.see('Pilih Penerbangan Pergi');
});
